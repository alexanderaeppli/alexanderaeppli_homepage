<?php get_header() ?>
<nav id="menu">
  <?php
		wp_nav_menu( array(
			'theme_location'    => 'primary',
			'depth'             => 2,
			'container'         => false,
			'container_id'      => 'bs-example-navbar-collapse-1',
      'menu_class'        => 'navbar fixed-top justify-content-md-center',
      'item_class'        => 'nav_item',
		) );
		?>
</nav>
<section id="start" class="container-fluid">
  <div id="particles-js"></div>
  <h1 id="title">
    <span class="black">Alexander</span>
    <span class="light">Aeppli</span>
  </h1>
  <p id="titletype"></p>
  <a id="scroll" class="scroll hidden" href="#content"><span></span></a>
</section>

<?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>
<div id="content" class="container">
  <?php the_content();?>

  <?php endwhile; else : ?>
  <p>
    <?php esc_html_e( 'Sorry, no posts matched your criteria.' ); ?>
  </p>
  <?php endif; ?>
  <?php get_footer() ?>