// Typeit config
new TypeIt('#titletype', {
  strings: ["Fotografie", "Layout", "Video", "Webdesign", '<span class="black">Mediamatiker</span>'],
  speed: 30,
  breakLines: false,
  autoStart: false,
  lifeLike: true,
  nextStringDelay: 1500,
});
// Typeit config end

// Smooth scroll @source: http://jsfiddle.net/9SDLw/
$('a').click(function () {
  $('html, body').animate({
    scrollTop: $($(this).attr('href')).offset().top-70
  }, 500);
  return false;
});
// Smooth scroll end

// Animated "back" buttons
function arrowEnter() {
  var arrow = document.getElementById("arrow");
  arrow.classList.add("hover");
}

function arrowLeave() {
  var arrow = document.getElementById("arrow");
  arrow.classList.remove("hover");
}
// Animated "back" buttons end

//Hide scroll button
setTimeout(function () {
  document.getElementById('scroll').classList.remove('hidden');
}, 8000);
//Hide scroll button end

// particle.js
/* particlesJS.load(@dom-id, @path-json, @callback (optional)); */
particlesJS('particles-js',

  {
    "particles": {
      "number": {
        "value": 40,
        "density": {
          "enable": true,
          "value_area": 800
        }
      },
      "color": {
        "value": "#ffffff"
      },
      "shape": {
        "type": "circle",
        "stroke": {
          "width": 0,
          "color": "#000000"
        },
        "polygon": {
          "nb_sides": 5
        },
        "image": {
          "src": "img/github.svg",
          "width": 100,
          "height": 100
        }
      },
      "opacity": {
        "value": .6,
        "random": true,
        "anim": {
          "enable": true,
          "speed": 0.5,
          "opacity_min": 0.1,
          "sync": false
        }
      },
      "size": {
        "value": 8,
        "random": true,
        "anim": {
          "enable": false,
          "speed": 4,
          "size_min": 0.3,
          "sync": false
        }
      },
      "line_linked": {
        "enable": false,
        "distance": 120,
        "color": "#ffffff",
        "opacity": 0,
        "width": 0.2
      },
      "move": {
        "enable": true,
        "speed": 0.5,
        "direction": "none",
        "random": true,
        "straight": false,
        "out_mode": "out",
        "bounce": false,
        "attract": {
          "enable": false,
          "rotateX": 600,
          "rotateY": 600
        }
      }
    },
    "interactivity": {
      "detect_on": "canvas",
      "events": {
        "onhover": {
          "enable": true,
          "mode": "repulse"
        },
        "onclick": {
          "enable": false,
          "mode": "repulse"
        },
        "resize": true
      },
      "modes": {
        "grab": {
          "distance": 400,
          "line_linked": {
            "opacity": 1
          }
        },
        "bubble": {
          "distance": 250,
          "size": 20,
          "duration": 2,
          "opacity": 1,
          "speed": 3
        },
        "repulse": {
          "distance": 150,
          "duration": 0.4
        },
        "push": {
          "particles_nb": 4
        },
        "remove": {
          "particles_nb": 2
        }
      }
    },
    "retina_detect": true
  }

);
// particle.js end

// Scrollspy
(function() {
  'use strict';

  var section = document.querySelectorAll(".section");
  var sections = {};
  var i = 0;

  Array.prototype.forEach.call(section, function(e) {
    sections[e.id] = e.offsetTop;
  });

  window.onscroll = function() {
    var scrollPosition = document.documentElement.scrollTop || document.body.scrollTop;

    for (i in sections) {
      if (sections[i] <= scrollPosition) {
        document.querySelector('.active').setAttribute('class', ' ');
        document.querySelector('a[href*=' + i + ']').setAttribute('class', 'active');
      }
    }
  };
});
// Scrollspy end

// Portfolio Filter AJAX
jQuery(function($){
	$('#filter').submit(function(){
		var filter = $('#filter');
		$.ajax({
			url:filter.attr('action'),
			data:filter.serialize(), // form data
			type:filter.attr('method'), // POST
			beforeSend:function(xhr){
				filter.find('button').text('Processing...'); // changing the button label
			},
			success:function(data){
				filter.find('button').text('Apply filter'); // changing the button label back
				$('#response').html(data); // insert data
			}
		});
		return false;
	});
});
